# About "Walkme"

WalkMe’s Digital Adoption Platform (DAP) makes it effortless to use any
software, website, or app. Combined with proactive, step-by-step guidance,
our comprehensive solution analyzes and automates processes so users can
complete tasks easily in the moment of need.

## Table of contents

- Installation
- Configuration
- Maintainers

## Installation

Install the Walkme module as you would normally install a
contributed Drupal module. visit https://www.drupal.org/node/1897420 for
further information.

## Configuration

- Before installing this module go to walkme.com,
- generate the script and publish script.
- After module is installed navigate to
- admin/config/system/walkme and place walkme script in Script field..

## Maintainers

- Raghunath Avula - [raghunatha](https://www.drupal.org/u/raghunatha)
- Poornima Koganti - [kpoornima](https://www.drupal.org/u/kpoornima)
