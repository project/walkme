<?php

namespace Drupal\walkme\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WalkmeForm.
 *
 * @package Drupal\walkme\Form
 */
class WalkmeForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'walkme.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'walkme_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('walkme.settings');

    $form['walkme_scope'] = [
      '#type' => 'select',
      '#title' => ('Script Scope'),
      '#options' => [
        'header' => t('Header'),
        'footer' => t('Footer'),
      ],
      '#default_value' => $config->get('walkme_scope', ''),
      '#description' => t('Where do you want to please walkme, header or footer.'),
    ];

    $form['walkme_script'] = [
      '#title' => t('Script'),
      '#type' => 'textarea',
      '#description' => t('Published script from WalkMe. Add script with out script tags.'),
      '#default_value' => $config->get('walkme_script', ''),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('walkme.settings')
      ->set('walkme_scope', $form_state->getValue('walkme_scope'))
      ->set('walkme_script', $form_state->getValue('walkme_script'))
      ->save(TRUE);
  }

}
